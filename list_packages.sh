#!/usr/bin/env bash

$DISCLAIMER="These are what is literally in the main/restricted archive.  This doesn't include caveats or packages that may have multiple uses where one is supported, but another isn't"

MIRROR=http://archive.ubuntu.com/ubuntu/ubuntu/dists/

for release in "bionic" "xenial"
do 
	for pocket in "restricted" "main"
	do
		rm Packages.xz Packages
		wget $MIRROR/$release/$pocket/binary-amd64/Packages.xz
		unxz Packages.xz
		grep -e "Package: " -e "Description: " -e "Supported: " Packages | sed -z "s/Package: //g; s/\nDescription: /,\"/g; s/\nSupported: /\",/g; "  > $release-$pocket.csv
	done
done

# vim: set et ts=4 sw=4 :
