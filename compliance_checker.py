#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import getopt
from launchpadlib.launchpad import Launchpad
from datetime import datetime
boiler_plate = """
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 30%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>"""


# For auth
def no_credential():
    print("Can't proceed without Launchpad credential.")
    sys.exit('2')


def turn_set_into_report(userlist, markwith, output_format):
    if(output_format == "text"):
        for user in userlist:
            print(markwith + '\t' + user)
    elif(output_format == "html"):
        for user in userlist:
            print("<tr><td>" + user + "</td><td>" + markwith + "</td></tr>")
    else:
        print("Error: output format can't be " + output_format)


def main(argv):
    target_team = ''
    bug_number = ''
    output_format = "text"
    auth = False
    try:
        opts, args = getopt.getopt(argv, "hat:b:", ["team=", "bug=", "auth", "html"])
    except getopt.GetoptError:
        print('test.py (-html) (-a/--auth) -t <teamname> -b <bug#>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('test.py -t <teamname> -b <bug#>')
            sys.exit()
        elif opt in ("-t", "--team"):
            target_team = arg
        elif opt in ("-b", "--bug"):
            bug_number = arg
        elif opt in ("--text"):
            output_format = "text"
        elif opt in ("--html"):
            output_format = "html"
        elif opt in ("-a", "--auth"):
            auth = True

    if(auth):
        launchpad = Launchpad.login_with(
            'compliance_checker', 'production', credential_save_failed=no_credential, version='devel')
    else:
        launchpad = Launchpad.login_anonymously('compliance_checker', 'production', "~/.cache/launchpadlib/", version='devel')

    # Create a list for users on a team
    team = launchpad.people(target_team)
    members = team.members_details

    members_list = []
    for member in members:
        members_list.append(member.member.name)
        # Note: Future things we might want to collect
        # print(member) returns https://api.launchpad.net/devel/~canonical-support/+member/bryanquigley
        # print(member.member.display_name)

    # Create a list for users on affected by a bug
    aff_users_list = []
    bug = launchpad.bugs[bug_number]
    users_affected = bug.users_affected

    for user in users_affected:
        aff_users_list.append(user.name)

    nowitis = datetime.now()
    if(output_format == "text"):
        print('Target team is ', target_team)
        print('Bug number is ', bug_number)
        print("Today's datetime is ", nowitis)
    elif(output_format == "html"):
        print(boiler_plate)
        print('<table>')
        print('<tr><td>Report runtime</td><td>' + str(nowitis) + '</td></tr>')
        print('<tr><td>Target team</td><td>' + target_team + '</td></tr>')
        print('<tr><td>Bug number</td><td>' + bug_number + '</td></tr>')
        print('<tr><td>Bug Title</td><td>' + bug.title + '</td></tr>')
        print('<tr><td>Bug Created</td><td>' + str(bug.date_created) + '</td></tr>')
        print('<tr><td>Bug Description</td><td>' + bug.description + '</td></tr>')
        print('</table><table>')
        print('<tr><td><strong>Launchpad Username</strong></td><td><strong>Agreed</strong></td></tr>')
    else:
        print("Error: output format can't be " + output_format)
    turn_set_into_report(set(aff_users_list) & set(members_list), "Y", output_format)
    turn_set_into_report(set(members_list) - set(aff_users_list), "N", output_format)
    if(output_format == "html"):
        print("</table>")


if __name__ == "__main__":
    main(sys.argv[1:])

# vim: set et ts=4 sw=4 :
