#lp_bikeshed
Random launchpad.net tools and scripts

#compliance_checker.py
Tool designed to compare the list of users affected by a bug to a list of users in a team.

This can be used for the users in that group to asset "compliance" to something.

Something like:
./compliance_checker.py -a --html -t canonical-support -b 1768029 > output.html

-a (auth as 1768029 is private)

#list_packages.sh
First pass on giving a list of packages in main.  Yes, this doesn't use lp, but heh.

